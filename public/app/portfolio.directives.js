app.directive('pageHeader', function(){
   return {
      templateUrl: '../templates/page-header.html'
   };
});
app.directive('pageFooter', function(){
	return {
		templateUrl: '../templates/page-footer.html'
	};
});

// A list of projects.
app.directive('projectList', function(){
	return {
		templateUrl: '../templates/project-list.html'
	};
});

// A list of a project's lang.
app.directive('projectLang', function(){
	return {
		templateUrl: '../templates/project-lang.html'
	};
});

// A list of a project's tech.
app.directive('screenShots', function(){
   return {
      templateUrl: '../templates/screen-shots.html'
   };
});

// A list of a project's tech.
app.directive('projectTech', function(){
	return {
		templateUrl: '../templates/project-tech.html'
	};
});

app.directive('skillRating', function(){
	return {
		templateUrl: '../templates/skill-rating.html'
	};
});

// And individual project listing.
app.directive('projectListing', function(){
	return {
		templateUrl: '../templates/project-listing.html'
	};
});
