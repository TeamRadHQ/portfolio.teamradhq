<?php
namespace Api;

class FolioSkill {
	protected $title;
	protected $rating = 0;
	protected $category = 0;
	/**
	 * Sets the title, rating and skill category.
	 * @param string  $title    The name of the skill.
	 * @param integer $rating   The rating out of five.
	 * @param integer $category The category id.
	 */
	public function __construct($title, $rating = 3, $category = 0) {
		$this->title    = $title;
		$this->rating   = ($rating > 0 && $rating <= 5 ? $rating : 2);
		$this->category = $category;
	}

	/**
	 * Casts object to array.
	 * @return array
	 */
	public function toArray() {
		return array(
			"title"    => $this->title,
			"rating"   => $this->rating,
			"category" => $this->category,
		);
	}
	/**
	 * Casts object to JSON.
	 * @return JSON
	 */
	public function toJson() {
		return json_encode($this->toArray());
	}
}