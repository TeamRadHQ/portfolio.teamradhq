var folio;
var app = angular.module('portfolioApp', ['ui.router']);

app.config(function($locationProvider, $stateProvider, $urlRouterProvider){
	$locationProvider.html5Mode(true);

	$urlRouterProvider.otherwise('/');
	
	$stateProvider.state('home', {
		url: '/',
		templateUrl: '../templates/portfolio.html',
		controller: 'portfolioController as folio',
		data: {
			title: 'Portfolio'
		}
	});
	$stateProvider.state('project', {
		url: '/project/:slug',
		templateUrl: '../templates/project-single.html',
		controller: 'projectController as folio',
		data: {
			title: 'Project'
		}
	});	
   $stateProvider.state('category', {
      url: '/:slug',
      templateUrl: '../templates/project-list.html',
      controller: 'categoryController as folio',
      data: {
         title: 'Portfolio'
      }
   });
});

