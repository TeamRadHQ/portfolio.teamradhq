<section class="rating" title="Rating: {{$skill->rating}} / 5">
    @foreach([1,2,3,4,5] as $score) 
        @if($skill->rating >= $score)
            <div class="circle rated"></div>
        @else
            <div class="circle"></div>    
        @endif
    @endforeach
</section>