
<table class="table container table-responsive table-striped table-hover">
    <thead>
        <tr> 
            <th class="">Skill</th>
            <th class=" text-center">Rating</th>
            <th class=" text-right">Action</th>
        </tr>
    </thead>
    <tbody>
@if($skills->count())
    @foreach($skills as $skill)
        <tr>
            <td class="col-xs-5"> {{$skill->name}} </td>
            <td class="col-sm-1 col-xs-2 text-center"> 
                @include('partials.rating')
            </td>
            <td class="col-xs-5 text-right">
                @include('partials.edit-skills-form')
                @include('partials.delete-skills-form')
            </td>
        </tr>
        <?php unset($skill); ?>
    @endforeach
@else 
    <tr> <td colspan="3">No Skills Added</td></tr>
@endif
    </tbody>
        <tfoot>
            <tr>
                <td colspan="3" class="button">
                    <a class="btn btn-primary btn-block">
                        Add a skill to {{$category->name}}
                    </a>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="form {{$category->id}} bg-info">
                {!! Form::open( array(
                    'method' => 'POST',
                    'url' => route('edit.skill.store'),
                    'class' => 'form'
                )) !!}
                    @include('partials.skills-form')    
                {!!Form::close()!!}
                </td>
            </tr>
        </tfoot>
    </tbody>
</table>
