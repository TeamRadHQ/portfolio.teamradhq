{{ Form::open([
          'url' => route('edit.skill.destroy', ["id" => $skill->id]),
          'method' => 'delete'
            ])}}

        <button type="submit" class="btn btn-danger" title="Delete Skill: {{$skill->name}}">
            <span class="glyphicon glyphicon-trash"></span>
        </button>
{{ Form::close() }}