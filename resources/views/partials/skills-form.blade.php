<?php
$skill = (isset($skill) ? $skill : new Portfolio\Skill);
?>
<p class="form-group">
    <input class="form-control" name="name" type="text" placeholder="Add to {{$category->name}}" value="{{$skill->name}}">
    <input type="hidden" name="category_id" value="{{$category->id}}">
</p>
<div class="row">
@foreach([1,2,3,4,5] as $rating)
	<p class="form-group col-xs-push-1 col-xs-2">
	    <input type="radio" name="rating" value="{{$rating}}"
			@if($skill->rating == $rating || ( ! $skill->rating && $rating == 3 ) )
				checked
			@endif
	    > {{$rating}}
	</p>
@endforeach
</div>
<p class="form-group col-sm-10 col-xs-9">
    <input type="submit" class="btn btn-primary btn-block" value="Save">
</p>
<p class="form-group col-sm-2 col-xs-3">
    <a class="btn btn-danger btn-block close-form"><span class="glyphicon glyphicon-remove"></a>
</p>