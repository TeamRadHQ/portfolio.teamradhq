{{ Form::open([
          'url' => route('edit.project.destroy', ["id" => $project->id]),
          'method' => 'delete'
            ])}}

        <button type="submit" class="btn btn-danger" title="Delete Project: {{$project->name}}">
            <span class="glyphicon glyphicon-trash"></span>
        </button>
{{ Form::close() }}