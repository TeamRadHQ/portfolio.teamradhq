<!-- Button trigger modal -->
<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal-{{$skill->id}}"  title="Edit Skill: {{$skill->name}}">
  <span class="glyphicon glyphicon-pencil"></span>
</button>

<!-- Modal -->
<div class="modal fade" id="myModal-{{$skill->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><small>Edit Skill</small> {{$skill->name}}</h4>
      </div>
      <div class="modal-body">
        {!! Form::model($skill, array(
            'method' => 'PUT',
            'url' => route('edit.skill.update', $skill->id),
            'class' => 'form container col-xs-12'
        )) !!}
            @include('partials.skills-form')    
        {!!Form::close()!!}
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>