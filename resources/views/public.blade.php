<!DOCTYPE html>
<html ng-app="portfolioApp">

<head>
    <base href="/">
    <!-- Page meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Author" content="Paul Beynon">

    <!-- Angular UI -->
    <script src="/app/components/angular/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    
    <!-- Angular App -->
    <script src="/app/portfolio.js"></script>
    <script src="/app/portfolio.directives.js"></script>
    <script src="/app/portfolio.controller.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/4.2.0/normalize.min.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/app.css">

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <title>Portfolio</title>

</head>
<body ng-controller="portfolioController as folio" style="max-width: 1400px; margin: 0 auto;">
    <page-header></page-header>
    <main ui-view class="container-fluid" >
       
    </main>
    @if(Auth::check())
	<div id="edit-panel">
		<ul>
			<li><h5>Editor</h5></li>
			<li><a target="_self" href="{!!route('edit.index')!!}" title="Skills Editor">Skills</a></li>
			<li><a target="_self" href="{!!route('edit.project.index')!!}" title="Projects Editor">Projects</a></li>
		</ul>
	</div>
    @endif
    <page-footer></page-footer>
    

</body>

</html>
