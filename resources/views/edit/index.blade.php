@extends('layouts.app')

@section('title')
Skills
@endsection

@section('content')

<div class="container">
	<h2 class="row">Skills</h2>
	<div class="panel-group" id="skills-accordion" role="tablist" aria-multiselectable="false">
		@foreach($skillCategories as $category)
		<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="heading-{{$category->id}}">
				<header class="row">
					<h2 class="panel-title col-xs-8">
						<a role="button" data-toggle="collapse" data-parent="#skills-accordion" href="#collapse-{{$category->id}}" aria-expanded="false" aria-controls="collapse-{{$category->id}}">
		          			{{$category->name}}
		        		</a>
		      		</h2>
					<p class="text-right col-xs-4">
						<a role="button" data-toggle="collapse" data-parent="#skills-accordion" href="#collapse-{{$category->id}}" aria-expanded="false" aria-controls="collapse-{{$category->id}}">
							<span class="hidden-xs">Average Rating : {{$category->averageRating}}</span>
							<span class="visible-xs">Avg : {{$category->averageRating}}</span>
						</a>
					</p>
				</header>
		    </div>
		    <div id="collapse-{{$category->id}}" class="panel-collapse collapse {!!($category->id == 1 ? 'in' : '')!!}" role="tabpanel" aria-labelledby="heading-{{$category->id}}">
	    	    @include('partials.skills-table', ["skills" => $category->skills])
	    	</div>
		</div>
		@endforeach
	</div>
</div>

<script>
var $skillForms = $('tfoot');

$skillForms.each(function(){
    var $this = $(this),
        $btn  = $this.children().eq(0);
        $form = $this.children().eq(1);
    $form.hide();

    console.log($skillForms.length);

    // Show single form
    $btn.bind('click', { skillForms: $skillForms }, function(event) {
        $(this).siblings().show();
        $(this).hide();
    })

    // Close Form
    $('.close-form').bind('click', function(){
        var $this = $(this);
        console.log();
        $this.closest('tfoot').children().eq(0).show();
        $this.closest('tfoot').children().eq(1).hide();
    });
});

</script>
@endsection
