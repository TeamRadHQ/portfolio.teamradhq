@extends('layouts.app')

@section('title')
Projects
@endsection

@section('content')

<div class="container">
<h2 class="row">Projects</h2>
<ul class="nav nav-pills" role="tablist">
@foreach($projectTypes as $type)
<?php $class = ($type->id == 1 ? 'active' : ''); ?>

    <li role="presentation" class="{{$class}}">
    	<a href="#type-{{$type->id}}" aria-controls="type-{{$type->id}}" role="tab" data-toggle="tab">
    		{{$type->name}}
    	</a>
    </li>

@endforeach
</ul>

<br>
  <!-- Tab panes -->
<div class="tab-content">
@foreach($projectTypes as $type)
    <div role="tabpanel" class="tab-pane {!! ($type->id == 1 ? 'active' : '') !!}" id="type-{{$type->id}}">
		<div class="panel panel-primary">
		  <div class="panel-heading ">
	    	<h3>{{ $type->name }}</h3>
		  </div>
  		  <div class="panel-body">
			<p class="lead">{{$type->description}}</p>
		  </div>
			<table class="table table-responsive table-striped">
	            <thead>
	                <th class="col-xs-3">Name</th>
	                <th class="col-xs-7">Description</th>
	                <th class="col-xs-2">Actions</th>
	            </thead>
	            <tbody>
	                @foreach($type->projects as $project)
	                <tr>
	                    <td>
	                        {{ $project->name }}
	                    </td>
	                    <td>
	                        {{ $project->description }}
	                    </td>
	                    <td>
	                        <a href="{!! route('edit.project.edit', $project->slug)  !!}" type="button" class="btn btn-success" title="Edit Project: {{$project->name}}">
	                            <span class="glyphicon glyphicon-pencil"></span>
	                        </a>

	                        @include('partials.delete-project-form')
	                    </td>
	                </tr>
	                @endforeach
	            </tbody>
	        </table>
		</div>
	</div>
@endforeach
<a href="{!! route('edit.project.create') !!}" class="btn btn-block btn-primary btn-lg">
	<span class="glyphicon glyphicon-plus"></span> Add a New Project
</a>
</div>

@endsection
