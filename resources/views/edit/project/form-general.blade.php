<fieldset>
    <legend><span class="glyphicon glyphicon-pencil"></span> General Information</legend>
    <div class="form-group">
        <label for="project_type_id">Project Type</label>
        <select name="project_type_id" class="form-control">
            @foreach($projectTypes as $type)
            <option value="{{$type->id}}"
            @if($type->id === $project->project_type_id)
                selected
            @endif
            >{{$type->name}}
            </option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="name">Name</label>
        {!!Form::text('name', $project->name,["id"=>"name", "class"=>"form-control", "placeholder"=>"Enter a name for this project..."])!!}
    </div>

    <div class="form-group">
        <label for="name">Description</label>
        {!!Form::textarea('description', $project->description,["id"=>"description", "class"=>"form-control", "placeholder"=>"Describe the project...", "rows" => 5])!!}
    </div>
</fieldset>
