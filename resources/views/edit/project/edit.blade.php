@extends('layouts.app')

@section('title')
Edit {{$project->name}}
@endsection

@section('content')

<div class="container">
    <h2 class="container row">
    	Edit Project
    	<small>{{$project->name}}</small>
    </h2>
    <div class="panel panel-primary">
        <div class="panel-body">
            {{Form::model($project, [
                "url"    => route('edit.project.update', $project->slug),
                "method" => "PUT",
                "files"  => true,
            ])}}
            @include('edit.project.form')
            {{Form::close()}}  
        </div>
    </div>
</div>

</script>
@endsection
