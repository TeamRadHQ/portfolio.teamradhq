<legend><span class="glyphicon glyphicon-briefcase"></span> Project Skills </legend>
<p class="lead">Add any skills which are applicable to this project.</p>

<div class="container row">
	<fieldset class="col-sm-3 col-xs-6 row">
	    <legend><span class="glyphicon glyphicon-book"></span> Lang</legend>

	    @foreach($langs as $lang)
		    <p><strong>{{$lang->name}}</strong></p>
		    @foreach($lang->skills as $skill)
			    <div class="checkbox">    
			        <label>
			            <input 
			                type="checkbox" 
			                name="lang[]" 
			                value={{$skill->slug}}
			            @if(in_array($skill->name, $project->langArray))
			                checked
			            @endif
			            > {{$skill->name}}
			        </label>
			    </div>
	    	@endforeach
	    @endforeach

	</fieldset>

	<fieldset class="col-sm-9 col-xs-6 container">
	    <legend><span class="glyphicon glyphicon-cog"></span> Tech</legend>
	    
	    @foreach($techs as $cat)
	    <div class="col-sm-4 row">
	        @if( count($cat["skills"]) )
	            <p><strong>{{$cat["name"]}}</strong></p>
		            @foreach($cat["skills"] as $tech)
		                <div class="checkbox">    
		                    <label>
		                        <input 
		                            type="checkbox" 
		                            name="tech[]" 
		                            value={{ $tech["slug"] }}
		                        @if( in_array($tech["name"], $project["techArray"]) )
		                            checked
		                        @endif
		                        > {{ $tech["name"] }}

		                    </label>
		                </div>
		            @endforeach
	        	@endif
	    	</div>
	    @endforeach
	</fieldset>
</div>
