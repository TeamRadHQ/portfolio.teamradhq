<div>
	<ul class="nav nav-pills nav-justified" role="tablist">
		<li role="presentation" class="active">
			<a href="#info" aria-controls="info" role="tab" data-toggle="tab">
				<span class="glyphicon glyphicon-pencil"></span>	
				Info
			</a>
		</li>
		<li role="presentation">
			<a href="#features" aria-controls="features" role="tab" data-toggle="tab">
				<span class="glyphicon glyphicon-star"></span>	
				Features
			</a>
		</li>
		<li role="presentation">
			<a href="#links" aria-controls="links" role="tab" data-toggle="tab">
				<span class="glyphicon glyphicon-link"></span>
				Links
			</a>
		</li>
		<li role="presentation">
			<a href="#images" aria-controls="images" role="tab" data-toggle="tab">
				<span class="glyphicon glyphicon-picture"></span>
				Images
			</a>
		</li>
		<li role="presentation">
			<a href="#skills" aria-controls="skills" role="tab" data-toggle="tab">
				Skills
				<span class="glyphicon glyphicon-briefcase"></span>	
			</a>
		</li>
	</ul>
	<br>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active in" id="info">
			@include('edit.project.form-general')
		</div>
		<div role="tabpanel" class="tab-pane fade" id="features">
			@include('edit.project.form-items')
		</div>
		<div role="tabpanel" class="tab-pane fade" id="links">
			@include('edit.project.form-links')
		</div>
		<div role="tabpanel" class="tab-pane fade" id="images">
			@include('edit.project.form-images')
		</div>
		<div role="tabpanel" class="tab-pane fade" id="skills">
			@include('edit.project.form-skills')
		</div>
	</div>

	<button type="submit" class="btn btn-primary pull-right">Save Project</button>

</div>
