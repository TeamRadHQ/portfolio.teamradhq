<fieldset>
    <legend><span class="glyphicon glyphicon-picture"></span> Screenshots</legend>
    {{-- Add Four Image Fieldsets --}}
    @foreach([0,1,2,3] as $a)
		<?php $b=$a+1;?>
		<div class="row zebra">
		    <div class="col-sm-6">
				{{-- Image Title  --}}
			    <div class="form-group">
			        <label for="imageTitles[{{$a}}]">Title {{ $b }}</label>
			        {!!Form::text("imageTitles[$a]",
			        	(isset($project->images[$a]) ? $project->images[$a]->title : ''), 
			        	[
			        		"id"=>"imageTitles[$a]", 
			        		"placeholder"=>"Image {$b} Title", 
			        		"class"=>"form-control", 
					])!!}
			    </div>
				{{-- Image Upload  --}}
			    <div class="form-group">
			        <label for="images[{{$a}}]">Image {{ $b }}</label>
			        {!!Form::file("images[$a]", [ "id" => "images[$a]"])!!}
			    </div>
		    </div>
		    {{-- Image Caption --}}
		    <div class="col-sm-6">
		        <label for="images[{{$a}}]">Caption {{ $b }}</label>
			    <div class="form-group ">
			        {!!Form::textarea("imageCaptions[$a]",
			        	(isset($project->images[$a]) ? $project->images[$a]->caption : ''),
			        	[
			        		"id"=>"imageCaptions[$a]",
			        		"rows" => 4 ,
			        		"placeholder"=>"Describe this screenshot...", 
			        		"class"=>"form-control", 
			        ])!!}
			    </div>
			</div>
			{{-- Current Image Display --}}
			@if(isset($project->images[$a]))
				<div class="col-xs-12">
					<figure class="text-center">
						<img src="{{$project->images[$a]->picture->medium->url}}" alt="Image {{$a}}" class="img img-responsive img-rounded">
					</figure>
					@if($a < 3)
						<button type="submit" class="btn btn-primary pull-right">Save Project</button>
					@endif
				</div>
				@endif
		</div>
   @endforeach
</fieldset>
