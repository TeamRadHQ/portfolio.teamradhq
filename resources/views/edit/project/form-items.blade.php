<fieldset>
    <legend><span class="glyphicon glyphicon-star"></span> Project Features</legend>
	@foreach([0,1,2,3] as $a)
	<div>
	    <div class="form-group">
	        <label for="name">Feature {{$a+1}}</label>
            {!!Form::text("listItem[$a]", 
            (isset($project->listItems[$a]) ? $project->listItems[$a]->list_item : ''),
            [
            	"class"       => "form-control", 
            	"placeholder" => "Key points about this project..."
           	])!!}
	    </div>
	</div>
    @endforeach
</fieldset>
