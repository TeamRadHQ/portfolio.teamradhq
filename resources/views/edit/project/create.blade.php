@extends('layouts.app')

@section('title')
New Project
@endsection

@section('content')

<div class="container">
    <h2 class="container row">New Project</h2>
    <div class="panel panel-primary">
        <div class="panel-body">
            {{Form::open([
                "url"    => route('edit.project.store'),
                "method" => "POST",
                "files"=>true,
            ])}}
            @include('edit.project.form')
            {{Form::close()}}  
        </div>
    </div>
</div>
@endsection
