<fieldset>
    <legend><span class="glyphicon glyphicon-link"></span> Links</legend>
    @foreach([0,1,2] as $a)
        <div class="row">
            <div class="form-group col-sm-6">
            	<label for="linkTitle[{{$a}}]">Link {{$a + 1}} Title</label>
                {!!Form::text("linkTitle[$a]", 
                	(isset($project->links[$a]) ? $project->links[$a]->title : ''),
                	[
                		"id"          => "linkTitle[$a]", 
                		"class"       => "form-control", 
                		"placeholder" => "Enter a link title..."
                ])!!}
            </div>
            <div class="form-group col-sm-6">
            	<label for="linkUrl[{{$a}}]">Link {{$a + 1}} Url</label>
                {!!Form::text("linkUrl[$a]", 
                	(isset($project->links[$a]) ? $project->links[$a]->url : ''),
                	[
                		"id"          => "linkUrl[$a]", 
                		"class"       => "form-control", 
                		"placeholder" => "Add the URL..."
                ])!!}
            </div>
        </div>
    @endforeach
</fieldset>
