@extends('layouts.app')

@section('content')
<div class="container">
	<h2>Login</h2>
    <div class="panel panel-default">             
        <div class="panel-body">
		    {{Form::open([
		        "url"    => url('/login'),
		        "method" => "POST",
		    ])}}
			    <div class="row container">
					<label for="email" class="col-xs-12 col-sm-2 control-label">Email</label>
					<div class="col-sm-8 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						{!!Form::email('email', old('email'), ['class'=>'form-control', 'id'=>'email'])!!}
					    @if ($errors->has('email'))
					        <span class="help-block">
					            <strong>{{ $errors->first('email') }}</strong>
					        </span>
					    @endif
					</div>
			    </div>
			    <div class="row container">
				<label for="password" class="col-xs-12 col-sm-2 control-label">Password</label>
					<div class="col-sm-8 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						{!!Form::password('password', ['class'=>'form-control', 'id'=>'password'])!!}
					    @if ($errors->has('password'))
					        <span class="help-block">
					            <strong>{{ $errors->first('password') }}</strong>
					        </span>
					    @endif
					</div>			    	
			    </div>


				<div class="checkbox col-sm-10 col-sm-push-2">
				    <label>
				        <input type="checkbox" name="remember"> Remember Me
				    </label>
				</div>
				<div class="form-group">
					<button type="submit" class="btn col-sm-2 btn-primary pull-right">
						Login
					</button>
				</div>
			{{Form::close()}}  
		</div>
	</div>
</div>
@endsection
