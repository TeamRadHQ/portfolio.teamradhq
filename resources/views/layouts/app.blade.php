<!DOCTYPE html>
<html lang="en">
<head>
    <base href="/">
    <!-- Page meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Author" content="Paul Beynon">

    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto+Mono:100,100i,300,300i,400,400i,500,500i,700,700i|Roboto+Slab:100,300,400,700|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/app.css">

    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/bootstrap.min.css" crossorigin="anonymous">
	<title>@yield('title') - Portfolio</title>
</head>
<body id="app-layout" class="user">
    <style>
        td > form {
            display: inline;
        }
        td.form {
            border-top: 2px solid #ccc !important;
            border-bottom: 3px solid #ccc !important;
        }
    </style>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header container">
                <a class="col-xs-4 navbar-brand text-left" href="{{ url('/') }}">
                    Portfolio
                </a>
                <a class ="col-xs-4 navbar-brand text-center" href="{{ url('/edit') }}">Skills</a>
                <a class ="col-xs-4 navbar-brand text-right" href="{{ url('/edit/project') }}">Projects</a>
            </div>
        </div>
    </nav>
    @yield('content')
    <nav class="navbar navbar-inverse navbar-fixed-bottom">
        <div class="container">
            <div class="navbar-left container">
                <!-- Branding Image -->
                <a class="navbar-brand col-xs-4" href="{{ url('/design') }}">
                    Design
                </a>
                <a class="navbar-brand col-xs-4 text-center" href="{{ url('/development') }}">
                    Dev<span class="hidden-xs">elopment</span>
                </a>
                <a class="navbar-brand col-xs-4 text-right" href="{{ url('/documentation') }}">
                    Doc<span class="hidden-xs">umentation</span>
                </a>
            </div>
        </div>
    </nav>
    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>
