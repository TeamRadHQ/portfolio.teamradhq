app.controller('categoryController', function($http, $state) { 
	var category = this;
	category.api = '/api/project?category=' + $state.params.slug;
	category.projects = null;
   category.getProjects = function() {
		$http.get(category.api).then(function(response){
			// Save category information.
         category.information = response.data[0];
         // Set active type.
         $state.current.data.type = category.information.name;
         // Move projects and delete from info.
			category.projects = category.information.projects;
			delete category.information.projects;
		});
	};
	category.getProjects();
   category.getType = function() {
      return $state.current.data.type;
   };
});

app.controller('projectController', function($http, $state) { 
	var folio = this;
	folio.api = '/api/project/' + $state.params.slug;
	folio.project = null;
	folio.getProject = function() {
		$http.get(folio.api).then(function(response){
			// Set Project
         folio.project = [response.data];
         // Set Active Category
         $state.current.data.type = folio.project[0].type;
         console.log($state.current.data);
		});
	};
	folio.getProject();
});



app.controller('portfolioController', function($http, $state) {
	var folio = this;
	
	folio.api = '/api/project/';
	folio.projectCategories = null;
	folio.setCurrentCategory = function() {
		console.log($state.params.slug);
	};
	folio.setCurrentCategory();
	folio.ready = false;

	folio.getProjectCategories = function() {
		var api = '/api/project/';
		if ($state.params.slug) {
			api+="?slug=" + $state.params.slug;
		}
		$http.get('/api/project/').then(function(response){
			folio.projectCategories = response.data;
			folio.currentCategoryProjects();
			folio.ready = true;
		});
	};
	folio.getProjectCategories();

	folio.currentCategoryProjects = function() {
		folio.currentProjects = null;
		if (folio.projectCategories) {
			folio.projectCategories.forEach(function(category) {
				if (category.id == folio.currentCategory) {
					folio.currentProjects = category.projects;
				}
			});
		}
		return folio.currentProjects;
	};

	folio.pageTitle = function() {
		if ($state.current.data) {
			return $state.current.data.title;
		}
	};
   folio.getType = function() {
      return $state.current.data.type;
   };
});
