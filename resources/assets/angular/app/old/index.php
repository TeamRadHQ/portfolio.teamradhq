<?php 
namespace Api;
require_once('FolioProject.php');
require_once('FolioSkill.php');
use Api\FolioProject as Project;
use Api\FolioSkill as Skill;

header("Access-Control-Allow-Origin: *"); 

/**
 * Categories
 * 		0 => Design
 * 		1 => Development
 * 		3 => Documentation
 */


$data = [];
// Languages
$skill = new Skill('HTML', 5, 0);
$skills[] = $skill->toArray();
$skill = new Skill('CSS', 4, 0);
$skills[] = $skill->toArray();
$skill = new Skill('JavaScript', 3, 0);
$skills[] = $skill->toArray();
$skill = new Skill('Java', 2, 0);
$skills[] = $skill->toArray();
$skill = new Skill('TypeScript', 2, 0);
$skills[] = $skill->toArray();

$data["skills"] = $skills;

$projects = [];

$project = new Project('Bicycle Share');
$project->description("A website for sharing bicycles.")
		->listItem("Check out my list item.")
		->listItem("Check out my other list item.")
		->lang(['HTML', 'CSS'])
		->tech(['Polymer'])
		->link('Demo Site', 'http://teamradhq.com/bicycle.share')
		->link('Bitbucket', 'https://bitbucket.org/TeamRadHQ/bicycle.share')
		->link('Client Brief', 'http://'.$_SERVER["HTTP_HOST"].'/docs/bicycle.share-brief.pdf');
$projects[] = $project->toArray();

$project = new Project('Network Solutions');
$project->description("A tech website.")
		->listItem("Check out my list item.")
		->listItem("Check out my other list item.")
		->lang(['HTML', 'CSS'])
		->tech(['Polymer'])
		->link('Demo Site', 'http://teamradhq.com/network.solutions')
		->link('Bitbucket', 'https://bitbucket.org/TeamRadHQ/network.solutions')
		->link('Client Brief', 'http://'.$_SERVER["HTTP_HOST"].'/docs/network.solutions-brief.pdf');
$projects[] = $project->toArray();

$project = new Project('Yoko\'s Kitchen');
$project->description("A recipe blog.")
		->listItem("Check out my list item.")
		->listItem("Check out my other list item.")
		->lang(['HTML', 'CSS'])
		->link('Demo Site', 'http://teamradhq.com/yoko.dev')
		->link('Bitbucket', 'https://bitbucket.org/TeamRadHQ/yoko.dev');
$projects[] = $project->toArray();

$project = new Project('Zen Larder');
$project->description("A restaurant site.")
		->listItem("Check out my list item.")
		->listItem("Check out my other list item.")
		->lang(['HTML', 'CSS'])
		->link('Demo Site', 'http://teamradhq.com/zen.larder')
		->link('Bitbucket', 'https://bitbucket.org/TeamRadHQ/zen.larder')
		->link('Client Brief', 'http://'.$_SERVER["HTTP_HOST"].'/docs/zen.larder-brief.pdf');
$projects[] = $project->toArray();

$project = new Project('Cat Cafe');
$project->description("Sub-theme built upon Roots Sage for the Cat Cafe Melbourne.")
		->category(1)
		->listItem("Check out my list item.")
		->listItem("Check out my other list item.")
		->lang(['HTML', 'CSS'])
		->tech(['Wordpress', 'Roots Sage', 'Bootstrap', 'jQuery'])
		->link('Demo Site', 'http://shareit.teamradhq.com/')
		->link('Bitbucket', 'https://bitbucket.org/TeamRadHQ/shareit')
		->link('Validator', 'http://'.$_SERVER["HTTP_HOST"].'/docs/share.it-validator-report.pdf')
		->link('Site Proposal', 'http://'.$_SERVER["HTTP_HOST"].'/docs/share.it-website-proposal.pdf');
$projects[] = $project->toArray();

$project = new Project('Share It');
$project->description("A site for listing short term accommodations.")
		->category(1)
		->listItem("Check out my list item.")
		->listItem("Check out my other list item.")
		->lang(['HTML', 'CSS'])
		->tech(['Mustache', 'Bootstrap'])
		->link('Demo Site', 'http://shareit.teamradhq.com/')
		->link('Bitbucket', 'https://bitbucket.org/TeamRadHQ/shareit')
		->link('Validator', 'http://'.$_SERVER["HTTP_HOST"].'/docs/share.it-validator-report.pdf')
		->link('Site Proposal', 'http://'.$_SERVER["HTTP_HOST"].'/docs/share.it-website-proposal.pdf');
$projects[] = $project->toArray();

$project = new Project('TeamRad Forms & Validator');
$project->description("An api for creating forms and validating their input.")
		->category(1)
		->listItem("Check out my list item.")
		->listItem("Check out my other list item.")
		->lang(['PHP'])
		->tech(['Mustache', 'Bootstrap'])
		->link('Demo Site', 'http://shareit.teamradhq.com/')
		->link('Bitbucket', 'https://bitbucket.org/TeamRadHQ/shareit')
		->link('Validator', 'http://'.$_SERVER["HTTP_HOST"].'/docs/share.it-validator-report.pdf')
		->link('Site Proposal', 'http://'.$_SERVER["HTTP_HOST"].'/docs/share.it-website-proposal.pdf');
$projects[] = $project->toArray();

$project = new Project('Top 5');
$project->description("A site for listing and rating places.")
		->category(1)
		->listItem("Check out my list item.")
		->listItem("Check out my other list item.")
		->lang(['HTML', 'CSS'])
		->tech(['Laravel', 'Angular 1', 'Bootstrap', 'jQuery', 'RequireJS'])
		->link('Demo Site', 'http://top5.teamradhq.com')
		->link('Bitbucket', 'https://bitbucket.org/trhq/top5.dev')
		->link('Dev Report', 'http://'.$_SERVER["HTTP_HOST"].'/docs/top5.dev-top5.report.pdf')
		->link('Install Guide', 'http://'.$_SERVER["HTTP_HOST"].'/docs/top5.dev-install.instructions.pdf');
$projects[] = $project->toArray();

$data["projects"] = $projects;




// echo '<pre>',print_r($_SERVER),'</pre>';
echo json_encode($data);