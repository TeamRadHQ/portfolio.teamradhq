<?php
namespace Api;

class FolioProject {
	protected $title;
	protected $description;
	protected $category = 0;
	protected $listItems = [];
	protected $lang = [];
	protected $links = [];
	protected $tech = [];
	/**
	 * Sets the title and description.
	 * @param  string $title
	 * @param  string $description
	 * @return void
	 */
	public function __construct($title, $description = '') {
		$this->title = $title;
		if ( $description )
			$this->description = $description;
		return $this;
	}

	public function description($description) {
		$this->description = $description;
		return $this;
	}
	public function category($cat) {
		if ( is_int($cat) && in_array($cat, [0,1,2]) )
			$this->category = $cat;
		return $this;
	}
	/**
	 * Add a list item to the project.
	 * @param  mixed $li  The list item or an array of list items.
	 * @return void
	 */
	public function listItem($li) {
		if(is_array($li)) {
			$this->listItems = array_merge($this->listItems, $li);
		} else {
			$this->listItems[] = $li;
		}
		return $this;
	}

	/**
	 * Add a language to the project.
	 * @param  mixed $lang  The language or an array of languages.
	 * @return void
	 */
	public function lang($lang) {
		if(is_array($lang)) {
			$this->lang = array_merge($this->lang, $lang);
		} else {
			$this->lang[] = $lang;
		}
		sort($this->lang, SORT_NATURAL | SORT_FLAG_CASE);
		return $this;
	}
	/**
	 * Add a technology to the project.
	 * @param  mixed $tech  The technology or an array of technologies.
	 * @return void
	 */
	public function tech($tech) {
		if(is_array($tech)) {
			$this->tech = array_merge($this->tech, $tech);
		} else {
			$this->tech[] = $tech;
		}
		sort($this->tech, SORT_NATURAL | SORT_FLAG_CASE);	
		return $this;
	}

	/**
	 * Add a link to the project
	 * @param  string $title The link's title.
	 * @param  string $url   The link's url.
	 * @return void
	 */
	public function link($title, $url) {
		$this->links[] = ["title" =>$title, "url" => $url];
		return $this;
	}
	// Converts object to array.
	public function toArray() {
		return array(
			"title"       => $this->title,
			"description" => $this->description,
			"category"    => $this->category,
			"listItems"   => $this->listItems,
			"lang"        => $this->lang,
			"tech"        => $this->tech,
			"links"       => $this->links
		);
	}
}