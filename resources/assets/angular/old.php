<!DOCTYPE html>
<html ng-app="portfolioApp">

<head>
    <base href="/">
    <!-- Page meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Author" content="Paul Beynon">

    <!-- Angular UI -->
    <script src="/app/components/angular/angular.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    
    <!-- Angular App -->
    <script src="/app/portfolio.js"></script>
    <script src="/app/portfolio.directives.js"></script>
    <script src="/app/portfolio.controller.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto+Mono:100,100i,300,300i,400,400i,500,500i,700,700i|Roboto+Slab:100,300,400,700|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/app.css">

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <title>Portfolio</title>

<style>
    h1 { 
        font-weight: 100;
        font-size: 7.5rem;
    }
</style>

</head>
<body ng-controller="portfolioController as folio">
    <page-header></page-header>
    <main ui-view>
       
    </main>

<script>
    $(document).ready(function(){
        /**
         * 
         * @type {[type]}
         */
        var $articleSections = $('article > section, article > dl');
        $articleSections.hide();

        // Article Heading
        $('article > h3').bind('click', function(){
            var $this     = $(this),
                $siblings = $this.siblings('dl, section');
            if ( ! $siblings.is(':visible')) {
                $articleSections.fadeOut(75);    
                setTimeout(function(){
                    $siblings.toggle(175);
                }, 75);
            }
        });

        // Content sections
        var $contentSections = $('section.content');
        $contentSections.hide(); // Hide all
        $contentSections.eq(0).show(); // Show first section
        $contentSections.find('article').eq(0).find('dl, section').show(); // Show first article

        // Main Navigation.
        var $nav = $('nav.main');
        $nav.children().find('a').bind('click', function(){
            // Find the content section
            var $content = $('.content.' + this.className);
            // Show content if hidden.
            if( ! $content.is(':visible') ) {
                // Fade out visible section.
                $contentSections.fadeOut(75); 
                // Fade out visible article sections.
                $articleSections.fadeOut(75);    
                // Fade in selected section.
                setTimeout(function(){
                    $content.fadeIn(175);
                    // Show the first section's article content.
                    $content.find('article').eq(0).children('section, dl').show(200);
                }, 75);
                
            }
        });
    });
</script>
</body>

</html>