<!DOCTYPE html>
<html>

<head>
    <!-- Page meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Author" content="Paul Beynon">

    <!-- Angular UI -->
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    
    <!-- Angular App -->
    <script src="/app/portfolio.js"></script>
    <script src="/app/portfolio.directives.js"></script>
    <script src="/app/portfolio.controller.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto+Mono:100,100i,300,300i,400,400i,500,500i,700,700i|Roboto+Slab:100,300,400,700|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/app.css">

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <title>Portfolio</title>

<style>
    h1 { 
        font-weight: 100;
        font-size: 7.5rem;
    }
</style>

</head>

<body ng-controller="portfolioController as folio">
    <header class="container">
        <h1 class="col-xs-12 text-center">Portfolio</h1>
        <nav class="col-xs-12 main">
            <ul class="list-unstyled">
                <li><a class="design">Design</a></li>
                <li><a class="development">Development</a></li>
                <li><a class="documentation">Documentation</a></li>
            </ul>
        </nav>
    </header>
    <main ui-view class="container">
        <section class="design content">
            <header>
                <h2 class="section">Design</h2>
                <p class="lead text-right"> Converting client briefs in to HTML &amp; CSS site designs</p>
            </header>
            <article>
                <h3>Bicycle Share</h3>
                <section class="lead">
                    <p>An application which provides rated listings for places of business by category, location and user.</p>
                    <ul>
                        <li>User registration, place creation and billing.</li>
                        <li>Admin user management and invoice creation.</li>
                        <li>Single page public site using Angular JS.</li>
                        <li>Google Maps integration, including address lookup.</li>
                    </ul>
                </section>
                <dl class="list-unstyled">
                    <dt>Lang</dt>
                    <dd>HTML, CSS, JavaScript</dd>
                    <dt>Tech</dt>
                    <dd>Google Maps API, Google Maps API</dd>
                    <dt>Links</dt>
                    <dd>
                        <ul class="list-unstyled">
                            <li><a href="#">Demo Site</a></li>
                            <li><a href="#">Bitbucket</a></li>
                        </ul>
                    </dd>
                </dl>
            </article>            

            <article>
                <h3>Network Solutions</h3>
                <section class="lead">
                    <p>An application which provides rated listings for places of business by category, location and user.</p>
                    <ul>
                        <li>User registration, place creation and billing.</li>
                        <li>Admin user management and invoice creation.</li>
                        <li>Single page public site using Angular JS.</li>
                        <li>Google Maps integration, including address lookup.</li>
                    </ul>
                </section>
                <dl class="list-unstyled">
                    <dt>Lang</dt>
                    <dd>HTML, CSS, JavaScript</dd>
                    <dt>Tech</dt>
                    <dd>jQuery</dd>
                    <dt>Links</dt>
                    <dd>
                        <ul class="list-unstyled">
                            <li><a href="#">Demo Site</a></li>
                            <li><a href="#">Bitbucket</a></li>
                        </ul>
                    </dd>
                </dl>
            </article>

            <article>
                <h3>Yoko's Kitchen</h3>
                <section class="lead">
                    <p>An application which provides rated listings for places of business by category, location and user.</p>
                    <ul>
                        <li>User registration, place creation and billing.</li>
                        <li>Admin user management and invoice creation.</li>
                        <li>Single page public site using Angular JS.</li>
                        <li>Google Maps integration, including address lookup.</li>
                    </ul>
                </section>
                <dl class="list-unstyled">
                    <dt>Lang</dt>
                    <dd>HTML, CSS</dd>
                    <dt>Links</dt>
                    <dd>
                        <ul class="list-unstyled">
                            <li><a href="#">Demo Site</a></li>
                            <li><a href="#">Bitbucket</a></li>
                        </ul>
                    </dd>
                </dl>
            </article>
            
            <article>
                <h3>Zen Larder</h3>
                <section class="lead">
                    <p>An application which provides rated listings for places of business by category, location and user.</p>
                    <ul>
                        <li>User registration, place creation and billing.</li>
                        <li>Admin user management and invoice creation.</li>
                        <li>Single page public site using Angular JS.</li>
                        <li>Google Maps integration, including address lookup.</li>
                    </ul>
                </section>
                <dl class="list-unstyled">
                    <dt>Lang</dt>
                    <dd>HTML, CSS, JavaScript</dd>
                    <dt>Links</dt>
                    <dd>
                        <ul class="list-unstyled">
                            <li><a href="#">Demo Site</a></li>
                            <li><a href="#">Bitbucket</a></li>
                        </ul>
                    </dd>
                </dl>
            </article>

        </section>
        <section class="development content">
            <header>
                <h2 class="section">Development</h2>
                <p class="lead text-right"> Fun stuff with PHP, JavaScript and all the other bits </p>
            </header>
            <article>
                <h3>Top 5 Places</h3>
                <section class="lead">
                    <p>An application which provides rated listings for places of business by category, location and user.</p>
                    <ul>
                        <li>User registration, place creation and billing.</li>
                        <li>Admin user management and invoice creation.</li>
                        <li>Single page public site using Angular JS.</li>
                        <li>Google Maps integration, including address lookup.</li>
                    </ul>
                </section>
                <dl class="list-unstyled">
                    <dt>Lang</dt>
                    <dd>PHP, HTML, CSS, JavaScript</dd>
                    <dt>Tech</dt>
                    <dd>Laravel, Angular JS, Google Maps API</dd>
                    <dt>Links</dt>
                    <dd>
                        <ul class="list-unstyled">
                            <li><a href="#">Demo Site</a></li>
                            <li><a href="#">Bitbucket</a></li>
                        </ul>
                    </dd>
                </dl>
            </article>            

            <article>
                <h3>ShareIt</h3>
                <section class="lead">
                    <p>An application which allows people to list and browse short term accommodations.</p>
                    <ul>
                        <li>User registration, place creation.</li>
                        <li>Admin user management and invoice creation.</li>
                        <li>Single page public site using Angular JS.</li>
                        <li>Google Maps integration, including address lookup.</li>
                    </ul>
                </section>
                <dl class="list-unstyled">
                    <dt>Lang</dt>
                    <dd>PHP, HTML, CSS, JavaScript</dd>
                    <dt>Tech</dt>
                    <dd>Bootstrap, Google Maps API, jQuery, Mustache, TeamRad/Forms</dd>
                    <dt>Links</dt>
                    <dd>
                        <ul class="list-unstyled">
                            <li><a href="#">Demo Site</a></li>
                            <li><a href="#">Bitbucket</a></li>
                        </ul>
                    </dd>
                </dl>
            </article>

            <article>
                <h3>Top 5 Places</h3>
                <section class="lead">
                    <p>An application which provides rated listings for places of business by category, location and user.</p>
                    <ul>
                        <li>User registration, place creation and billing.</li>
                        <li>Admin user management and invoice creation.</li>
                        <li>Single page public site using Angular JS.</li>
                        <li>Google Maps integration, including address lookup.</li>
                    </ul>
                </section>
                <dl class="list-unstyled">
                    <dt>Lang</dt>
                    <dd>PHP, HTML, CSS, JavaScript</dd>
                    <dt>Tech</dt>
                    <dd>Laravel, Angular JS, Google Maps API</dd>
                    <dt>Links</dt>
                    <dd>
                        <ul class="list-unstyled">
                            <li><a href="#">Demo Site</a></li>
                            <li><a href="#">Bitbucket</a></li>
                        </ul>
                    </dd>
                </dl>
            </article>

            <article>
                <h3>Top 5 Places</h3>
                <section class="lead">
                    <p>An application which provides rated listings for places of business by category, location and user.</p>
                    <ul>
                        <li>User registration, place creation and billing.</li>
                        <li>Admin user management and invoice creation.</li>
                        <li>Single page public site using Angular JS.</li>
                        <li>Google Maps integration, including address lookup.</li>
                    </ul>
                </section>
                <dl class="list-unstyled">
                    <dt>Lang</dt>
                    <dd>PHP, HTML, CSS, JavaScript</dd>
                    <dt>Tech</dt>
                    <dd>Laravel, Angular JS, Google Maps API</dd>
                    <dt>Links</dt>
                    <dd>
                        <ul class="list-unstyled">
                            <li><a href="#">Demo Site</a></li>
                            <li><a href="#">Bitbucket</a></li>
                        </ul>
                    </dd>
                </dl>
            </article>

        </section>
        <section class="documentation content">
            <header>
                <h2 class="section">Documentation</h2>
                <p class="lead">Writing for clients, devs and users</p>
            </header>
            
            <article>
                <h3>Copyright</h3>
                <section class="lead">
                    <p>An application which provides rated listings for places of business by category, location and user.</p>
                    <ul>
                        <li>User registration, place creation and billing.</li>
                        <li>Admin user management and invoice creation.</li>
                        <li>Single page public site using Angular JS.</li>
                        <li>Google Maps integration, including address lookup.</li>
                    </ul>
                </section>
                <dl class="list-unstyled">
                    <dt>Lang</dt>
                    <dd>PHP, HTML, CSS, JavaScript</dd>
                    <dt>Tech</dt>
                    <dd>Laravel, Angular JS, Google Maps API</dd>
                    <dt>Links</dt>
                    <dd>
                        <ul class="list-unstyled">
                            <li><a href="#">Demo Site</a></li>
                            <li><a href="#">Bitbucket</a></li>
                        </ul>
                    </dd>
                </dl>
            </article>            

            <article>
                <h3>OHS &amp; Sustainability Policies</h3>
                <section class="lead">
                    <p>An application which provides rated listings for places of business by category, location and user.</p>
                    <ul>
                        <li>User registration, place creation and billing.</li>
                        <li>Admin user management and invoice creation.</li>
                        <li>Single page public site using Angular JS.</li>
                        <li>Google Maps integration, including address lookup.</li>
                    </ul>
                </section>
                <dl class="list-unstyled">
                    <dt>Lang</dt>
                    <dd>PHP, HTML, CSS, JavaScript</dd>
                    <dt>Tech</dt>
                    <dd>Laravel, Angular JS, Google Maps API</dd>
                    <dt>Links</dt>
                    <dd>
                        <ul class="list-unstyled">
                            <li><a href="#">Demo Site</a></li>
                            <li><a href="#">Bitbucket</a></li>
                        </ul>
                    </dd>
                </dl>
            </article>

            <article>
                <h3>Documentation</h3>
                <section class="lead">
                    <p>An application which provides rated listings for places of business by category, location and user.</p>
                    <ul>
                        <li>User registration, place creation and billing.</li>
                        <li>Admin user management and invoice creation.</li>
                        <li>Single page public site using Angular JS.</li>
                        <li>Google Maps integration, including address lookup.</li>
                    </ul>
                </section>
                <dl class="list-unstyled">
                    <dt>Lang</dt>
                    <dd>PHP, HTML, CSS, JavaScript</dd>
                    <dt>Tech</dt>
                    <dd>Laravel, Angular JS, Google Maps API</dd>
                    <dt>Links</dt>
                    <dd>
                        <ul class="list-unstyled">
                            <li><a href="#">Demo Site</a></li>
                            <li><a href="#">Bitbucket</a></li>
                        </ul>
                    </dd>
                </dl>
            </article>

            <article>
                <h3>Project Planning</h3>
                <section class="lead">
                    <p>An application which provides rated listings for places of business by category, location and user.</p>
                    <ul>
                        <li>User registration, place creation and billing.</li>
                        <li>Admin user management and invoice creation.</li>
                        <li>Single page public site using Angular JS.</li>
                        <li>Google Maps integration, including address lookup.</li>
                    </ul>
                </section>
                <dl class="list-unstyled">
                    <dt>Lang</dt>
                    <dd>PHP, HTML, CSS, JavaScript</dd>
                    <dt>Tech</dt>
                    <dd>Laravel, Angular JS, Google Maps API</dd>
                    <dt>Links</dt>
                    <dd>
                        <ul class="list-unstyled">
                            <li><a href="#">Demo Site</a></li>
                            <li><a href="#">Bitbucket</a></li>
                        </ul>
                    </dd>
                </dl>
            </article>

        </section>
    </main>

<script>
    $(document).ready(function(){
        /**
         * 
         * @type {[type]}
         */
        var $articleSections = $('article > section, article > dl');
        $articleSections.hide();

        // Article Heading
        $('article > h3').bind('click', function(){
            var $this     = $(this),
                $siblings = $this.siblings('dl, section');
            if ( ! $siblings.is(':visible')) {
                $articleSections.fadeOut(75);    
                setTimeout(function(){
                    $siblings.toggle(175);
                }, 75);
            }
        });

        // Content sections
        var $contentSections = $('section.content');
        $contentSections.hide(); // Hide all
        $contentSections.eq(0).show(); // Show first section
        $contentSections.find('article').eq(0).find('dl, section').show(); // Show first article

        // Main Navigation.
        var $nav = $('nav.main');
        $nav.children().find('a').bind('click', function(){
            // Find the content section
            var $content = $('.content.' + this.className);
            // Show content if hidden.
            if( ! $content.is(':visible') ) {
                // Fade out visible section.
                $contentSections.fadeOut(75); 
                // Fade out visible article sections.
                $articleSections.fadeOut(75);    
                // Fade in selected section.
                setTimeout(function(){
                    $content.fadeIn(175);
                    // Show the first section's article content.
                    $content.find('article').eq(0).children('section, dl').show(200);
                }, 75);
                
            }
        });
    });
</script>
</body>

</html>