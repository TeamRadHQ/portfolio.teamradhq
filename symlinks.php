#!/Applications/XAMPP/xamppfiles/bin/php
<?php
class SymLinker {
	public $links = [];
	protected $paths = array(
		"proj" => __DIR__,
		"ng"   => "/resources/assets/angular",
		"pub"  => "/public",
	);

	public function add($file, $link) {
		$this->links[] = array(
			"ng" => $file,
			"pub" => $link,
		);
		return $this;
	}
	public function doLinks() {
		foreach($this->links as $link) {
			$ng =  $this->ngPath($link["ng"]);
			$pub =  $this->pubPath($link["pub"]);
			if (file_exists($pub)) {
				unlink($pub);
			}
			echo "Linking: $ng\n";
			echo "     To: $pub\n";
			echo "========================================\n";
			shell_exec("ln -s $ng $pub");
		}
	}
	public function ngPath($file) {
		return $this->paths["proj"].$this->paths["ng"].$file;
	}
	public function pubPath($file) {
		return $this->paths["proj"].$this->paths["pub"].$file;
	}

}

$symLinks = new SymLinker;
/* Bootstrap */
$asset = "/app/components/bootstrap/dist/";
$symLinks->add( $asset."css/bootstrap.min.css", "/css/" )
		 ->add( $asset."js/bootstrap.min.js",  "/js/" )
		 ->add( $asset."fonts/glyphicons-halflings-regular.eot",   "/fonts/glyphicons-halflings-regular.eot" )
		 ->add( $asset."fonts/glyphicons-halflings-regular.svg",   "/fonts/glyphicons-halflings-regular.svg" )
		 ->add( $asset."fonts/glyphicons-halflings-regular.ttf",   "/fonts/glyphicons-halflings-regular.ttf" )
		 ->add( $asset."fonts/glyphicons-halflings-regular.woff",  "/fonts/glyphicons-halflings-regular.woff" )
		 ->add( $asset."fonts/glyphicons-halflings-regular.woff",  "/fonts/glyphicons-halflings-regular.woff" )
		 ->add( $asset."fonts/glyphicons-halflings-regular.woff2", "/fonts/glyphicons-halflings-regular.woff2" );

/* jQuery */
$symLinks->add( "/app/components/jquery/dist/jquery.min.js", "/js/jquery.min.js" );

/* Angular */
$symLinks->add( "/app/components/angular/angular.min.js", "/js/angular.min.js")
		 ->add( "/app/components/angular-ui-router/release/angular-ui-router.min.js", "/js/angular-ui-router.min.js")
		 ->add( "/app", "/app")
		 ->add( "/templates", "/templates");

/* Application */
$symLinks->add('/css/app.css', '/css/app.css');

$symLinks->doLinks();



