<?php
namespace Database\Seeds;
use Illuminate\Database\Seeder;
use Portfolio\Project;
use Portfolio\ProjectType as Type;
use Portfolio\ProjectLink as Link;
use Portfolio\ProjectListItem as Item;
use Portfolio\ProjectImage as Image;

trait NewProjectTrait
{
	protected $i = 0;
	protected $debug = true;
	protected $existing_uploads = '/app/uploads/user';
	protected $cleared = false;

	protected $project = null;
	protected $data = [];
	protected $list = [];
	protected $images = [];
	protected $links = [];
	protected $skills = [];
	protected $type = null;

	protected function initialise_uploads_directory() {
		if( ! $this->cleared) {
			$dir = storage_path() . $this->existing_uploads;
			if(file_exists($dir)) {
				shell_exec("rm -rf {$dir}");
			}
			$cleared = true;
		}
	}
	public function setType($name) {
		if ( isset($this->type->name) && $this->type->name == $name ) return;

		$this->type = Type::where('name', '=', $name)->first();
	}
	/**
	 * Makes a project and its relations the $data its passed. 
	 * @param  array  $data An associative array of project data.
	 * @return void
	 */
	public function make ($data) {
		$this->setType($data["type"]);
		echo "Making {$data['name']}...\n";
		$this->makeProject(
			$data["name"], 
			$data["description"]
		);
		echo "Adding {$data['name']} list items...\n";
		foreach ($data["list"] as $item) {
			$this->addList($item);
			
		}
		echo "Adding {$data['name']} links...\n";
		foreach($data["links"] as $link) {
			$title = ( isset($link["title"]) ? $link["title"] : $link[0] );
			$url   = ( isset($link["url"]) 	 ? $link["url"]   : $link[1] );
			$this->addLink( $title, $url);
		}
		echo "Adding {$data['name']} images...\n";
		foreach($data["images"] as $image) {
			$file    = ( isset($image["file"])    ? $image["file"]    : $image[0] );
			$title   = ( isset($image["title"])   ? $image["title"]   : $image[1] );
			$caption = ( isset($image["caption"]) ? $image["caption"] : $image[2] );
			$this->addImage( $file, $title, $caption);
		}
		echo "Adding {$data['name']} skills...\n";
		$this->addSkills($data["skills"]);
		$this->saveAndWipe();
	}
	/**
	 * Makes a new Project with $name and $description 
	 * and saves it to the database.
	 * @param  string $name 
	 * @param  string $description 
	 * @return $this
	 */
	public function makeProject($name, $description) {
		$this->project = new Project;
		$this->project->fill([
			"name"        => $name,
			"description" => $description,
		]);
		$this->type->projects()->save($this->project);
		$this->debugMsg("Created {$this->project->name}.");
		return $this;
	}
	/**
	 * Saves and wipes the current data set.
	 * @return $this
	 */
	public function saveAndWipe() {
		$this->debugMsg("Saving and wiping...");
		return $this->save()->wipe();
	}
	/**
	 * Saves the current data set to the database.
	 * @return $this
	 */
	public function save() {
		$this->debugMsg("Saving related models for {$this->project->name}.");
		$this->saveList();
		$this->saveLinks();
		$this->saveImages();
		$this->saveSkills();
		$this->project->save();
		return $this;
	}
	/**
	 * Wipes the current data set, in preparation for adding another project.
	 * @return void
	 */
	public function wipe() {
		$this->debugMsg("Wiping");
		$this->project = null;
		$this->data    = [];
		$this->list    = [];
		$this->images  = [];
		$this->links   = [];
		$this->skills  = [];
		return $this;
	}
	/**
	 * Adds a list item to the current data set.
	 * @param string $item
	 */
	public function addList($item) {
		$this->debugMsg("Adding item to {$this->project->name} : \n" . wordwrap($item, 40));
		array_push($this->list, $item);
	}
	/**
	 * Adds an image to the current data set.
	 * @param string $file
	 * @param string $title
	 * @param string $caption
	 */
	public function addImage($file, $title, $caption) {
		$this->debugMsg("Adding Image:$file for {$this->project->name}.");
		array_push($this->images, ["file" => $file, "title" => $title, "caption" => $caption]);
	}
	/**
	 * Adds a link to the current data set.
	 * @param string $title
	 * @param string $url
	 */
	public function addLink($title, $url) {
		$this->debugMsg("Adding new Link:$title for {$this->project->name}.");
		array_push($this->links, ["title" => $title, "url" => $url]);
	}
	/**
	 * Adds an array of skills to the current data set.
	 * @param array:string $skills
	 */
	public function addSkills(array $skills = array()) {
		$this->debugMsg("Adding " . count($skills) . " Skills for {$this->project->name}.");
		foreach($skills as $skill) {
			array_push($this->skills, $skill);
		}
	}
	/**
	 * Saves the current list data to the database.
	 * @return void
	 */
	public function saveList() {
		$this->debugMsg("Saving {$this->project->name} list.");
		if(count($this->list)) {
			foreach($this->list as $item) {
				$li = new Item;
				$li->list_item = $item;
				$this->project->listItems()->save($li);
			}
		}
	}
	/**
	 * Saves the current link data to the database.
	 * @return void
	 */
	public function saveLinks() {
		$this->debugMsg("Saving {$this->project->name} links.");
		if(count($this->links)) {
			foreach($this->links as $link) {
				$li = new Link;
				$li->title = $link["title"];
				$li->url   = $link["url"];
				$this->project->links()->save($li);
			}
		}
	}
	/**
	 * Saves the current skill data to the database.
	 * @return void
	 */
	public function saveSkills() {
		$this->debugMsg("Saving {$this->project->name} skills.");
		if(count($this->skills)) {
			foreach($this->skills as $skill) {
				$this->project->setSkill($skill);
			}
		}
	}
	/**
	 * Saves the current image data to the database.
	 * @return void
	 */
	public function saveImages() {
		$this->debugMsg("Saving {$this->project->name} images.");
		if(count($this->images)) {
			foreach($this->images as $key => $image) {
				$img = new Image;

				$img->title   = $this->images[$key]["title"];
				$img->caption = $this->images[$key]["caption"];
				$img->picture = env("APP_URL") . '/uploads/' . $this->images[$key]["file"];
				$this->project->images()->save($img);
			}
		}
	}
	/**
	 * Outputs a debug $msg if debug mode is on (true)
	 * @param  string $msg
	 * @return stdout
	 */
	protected function debugMsg($msg) {
		if($this->debug) {
			echo ($msg."\n");
			echo (str_repeat('=', 45)."\n");
		}
	}
}
