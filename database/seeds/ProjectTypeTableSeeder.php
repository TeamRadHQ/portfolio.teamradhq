<?php

use Illuminate\Database\Seeder;
use Portfolio\ProjectType as Type;
class ProjectTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Type::create([
        	"name"        => "Design",
        	"description" => "Converting client briefs in to HTML & CSS site designs",
        ]);
		Type::create([
        	"name"        => "Development",
        	"description" => "Fun stuff with PHP, JavaScript & all the other bits",
        ]);
		Type::create([
        	"name"        => "Documentation",
        	"description" => "Writing for clients, devs and users",
        ]);
        $types = Type::all();
        $types->each(function($type) { $type->resluggify()->save();});
    }
}
