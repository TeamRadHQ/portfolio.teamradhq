<?php

use Illuminate\Database\Seeder;
use Portfolio\Skill as Skill;
use Portfolio\SkillCategory as Category;

class SkillsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->cat = Category::create([
			"name"        => "Languages",
			"description" => "Computer languages."
		]);
			$this->saveSkills(array(
				["HTML",5],
				["CSS",4],
				["SASS",3],
				["ES6",2],
				["Stylus",4],
				["JavaScript",3],
				["PHP",4],
				["SQL",3],
				["Java",2],
				["TypeScript",2],
			));
		$this->cat = Category::create([
			"name"        => "Communication",
			"description" => "Professional Writing."
		]);
			$this->saveSkills(array(
				["Documentation",5],
				["Policy Writing",3],
				["Process Diagrams",4],
				["Technical Writing",3],
				["Test Plans",3],
			));

		/* Frameworks & Tools. */
		$this->cat = Category::create([
			"name"        => "Frameworks & Tools",
			"description" => "Tools used in development."
		]);
		echo "Adding skills for {$this->cat->name}...\n";
			$this->saveSkills(array(
				["jQuery", 3],
				["RequireJS", 3],
				["Laravel", 3],
				["Bootstrap", 4],
				["Slim", 3],
				["Google Maps API", 3],
				["WordPress", 4],
				["Angular 1", 3],
				["Angular 2", 2],
				["Polymer", 2],
			));

		/* Dev Tools */
		$this->cat = Category::create([
			"name"        => "Dev Tools",
			"description" => "Version control, dependency managers & task runners."
		]);
		echo "Adding skills for {$this->cat->name}...\n";
			$this->saveSkills(array(
				["NPM", 3],
				["Git", 3],
				["Bower", 3],
				["Composer", 2],
				["Gulp", 3],
				["Node.js", 2],
				["Vagrant", 2],
			));

		/* Computing */
		$this->cat = Category::create([
			"name"        => "Computing",
			"description" => "Providing technological solutions."
		]);
		echo "Adding skills for {$this->cat->name}...\n";
			$this->saveSkills(array(
				["Apache", 3],
				["Linux Administration", 2],
			));

		/* Software */
		$this->cat = Category::create([
			"name"        => "Software",
			"description" => "Commercial software packages."
		]);
		echo "Adding skills for {$this->cat->name}...\n";
			$this->saveSkills(array(
				["Photoshop CC", 3],
				["Premiere Pro CC", 3],
				["MS Word", 4],
				["MS Excel", 4],
				["Ableton Live", 4],
				["NI Maschine", 4],
			));

		/* Professional Services */
		$this->cat = Category::create([
			"name"        => "Professional Services",
			"description" => "Professional services and support for stakeholders."
		]);
		echo "Adding skills for {$this->cat->name}...\n";
			$this->saveSkills(array(
				["Client Services", 4],
				["IT Support", 4],
				["OHS", 4],
				["ICT Practice", 3],
				["Training", 3],
				["Business Admin", 3],
			));

		/* Creative Media */
		$this->cat = Category::create([
			"name"        => "Creative Media",
			"description" => "Sound and vision."
		]);
		echo "Adding skills for {$this->cat->name}...\n";
			$this->saveSkills(array(
				["Photography", 2],
				["Audio Production", 4],
				["Video Editing", 3],
				["Document Design", 4],
			));
	 
		echo "Done adding skills...\n";
	}
	public function saveSkills($skills) {
		foreach($skills as $skill) {
			$this->saveSkill($skill[0], $skill[1]);
		}
	}
	public function saveSkill($name, $rating) {
			$skill = new Skill;
			$skill->fill([
				"name"        => $name,
				"normalised"  => strtolower($name),
				"rating"      => $rating,
			]);
			$this->cat->skills()->save($skill);
	}
	public function sluggify() {
		$categories = Category::all();
		$categories->each(function($category) {
			$category->resluggify()->save();
		});
		/* Sluggify Records */
		$cats = Category::all();
		$cats->each(function($cat) {
			$cat->resluggify()->save();
		});

		$skills = Skill::all();
		$skills->each(function($skill) {
			$skill->resluggify()->save();
		});
	}
}
