<?php

use Illuminate\Database\Seeder;
use Portfolio\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// dd(env('APP_USER_NAME'));
        $user = new User;
        $user->name     = env('APP_USER_NAME');
        $user->email    = env('APP_USER_EMAIL');
        $user->password = Hash::make(env('APP_SECRET'));
        $user->save();
    }
}
