<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		
		echo "Preparing to seed Users...\n";
		$this->call(UserTableSeeder::class);
		echo "Preparing to seed Types...\n";
		$this->call(ProjectTypeTableSeeder::class);
		echo "Preparing to seed Categories...\n";
		$this->call(SkillCategoriesTableSeeder::class);
		echo "Preparing to seed Skills...\n";
		$this->call(SkillsTableSeeder::class);
		echo "Preparing to seed Project...\n";
		$this->call(ProjectsTableSeeder::class);

	}
}
