<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_skills', function (Blueprint $table) {
            $table->unsignedInteger('skill_id');
            $table->unsignedInteger('skillable_id');
            $table->integer('skillable_type')->unsigned();
            $table->timestamps();
        });
        Schema::table('project_skills', function (Blueprint $table) {
            $table->foreign('skill_id')
                  ->references('id')
                  ->on('skills')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_skills', function (Blueprint $table) {
            $table->dropForeign('project_skills_skill_id_foreign');
        });
        Schema::drop('project_skills');
    }
}
