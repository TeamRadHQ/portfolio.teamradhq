<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_images', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('project_id');
            $table->text('picture');
            $table->string('title');
            $table->string('caption')->nullable();
            $table->timestamps();
        });
        Schema::table('project_images', function (Blueprint $table) {
            $table->foreign('project_id')
            	->references('id')
            	->on('projects')
            	->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_images', function (Blueprint $table) {
	        $table->dropForeign('project_images_project_id_foreign');
        });
        Schema::drop('project_images');
    }
}
