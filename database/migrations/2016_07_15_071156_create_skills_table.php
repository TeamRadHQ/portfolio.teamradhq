<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->string('name')->unique();
            $table->tinyInteger('rating')->unsigned();
            $table->string('normalised');
            $table->string('slug');
            $table->timestamps();
        });

        // Add foreign key to skills.
        Schema::table('skills', function (Blueprint $table) {
            $table->foreign('category_id')
                  ->references('id')
                  ->on('skill_categories')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('skills', function (Blueprint $table) {
            $table->dropForeign('skills_category_id_foreign');
        });
        Schema::drop('skills');
    }
}
