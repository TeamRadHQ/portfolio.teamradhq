<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->integer('project_type_id')->unsigned();
            $table->string('slug')->nullable();
            $table->timestamps();
        });
        Schema::table('projects', function (Blueprint $table) {
            $table->foreign('project_type_id')
                  ->references('id')
                  ->on('project_types')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropForeign('projects_project_type_id_foreign');
        });
        Schema::drop('projects');
    }
}
