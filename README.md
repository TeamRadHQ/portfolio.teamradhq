# Online Portfolio

This is a Laravel / Angular application for presenting an online web development portfolio. 

The finished product will provide the following:

* Skills, categorised and rated (e.g. Computer Languages, Professional Services etc.)

* Projects including description, language and tech overview, project links and screenshots. 

## TODO
Focus on Docs first, once compiled, do the same with screenshots.
###DOCS
* Sustainability Policy 
    - Fix headers and footers
* OHS Policy
    - Review and Approve
* Share It Development Proposal
    - Review and Approve
* TBM CMS Concept
* Cat Cafe Test Plan
    - Review and Approve
* Top 5 Application Report
    - Review and Approve
* Royal Beans Case Study 
    - Need to remove some sections.
    - Update template
* Team Rad Validator 
    - Need to fix headers and titles.
* Photomasters Sales Pitch 
    - Need to fix the layout
    - Language needs chaging
