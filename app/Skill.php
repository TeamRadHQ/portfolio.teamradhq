<?php

namespace Portfolio;

use Illuminate\Database\Eloquent\Model;

use Portfolio\SkillCategory as Category;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;



class Skill extends Model
{
	use SluggableTrait;
	
	/**
     * Makes Project Type sluggable.
     * @var array
     */
    protected $sluggable = array(
        'build_from' => 'name',
        'save_to' => 'slug',
        'on_update' => true,
    );

    protected $fillable = [
        'name', 'description', 'slug', 'normalised'
    ];
    protected $dates = ['created_at', 'updated_at'];

    public function category() {
    
    	return $this->belongsTo('Portfolio\SkillCategory');
    }

    public function projectSkill() {
    
        return $this->hasMany('Portfolio\ProjectSkill');
    }

    public function setNameAttribute($value) {
        $value = trim($value);
        $this->attributes['name'] = $value;
        $this->attributes['normalised'] = $value;
    }

}
