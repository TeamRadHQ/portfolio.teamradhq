<?php

namespace Portfolio\Http\Requests;

use Portfolio\Http\Requests\Request;

class SkillsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->input('name', ucwords($this->get('name')));
        $rules = [
            'name'        => 'required|unique:skills',
            'rating'      => 'required',
            'category_id' => 'required',
        ];
        switch ($this->method() ) {
            case 'GET':
                return[];
            case 'DELETE':
                return[];
            case 'POST':
                return $rules;
            case 'PUT':
                return $rules;
            case 'PATCH':
                return $rules;
        }
    }
}
