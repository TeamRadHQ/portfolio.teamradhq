<?php

/* Angular Index */
Route::get('/', function () {
    return view('public');
});
Route::get('/design', function () {
    return redirect('/#/design');
});
Route::get('/development', function () {
    return redirect('/#/development');
});
Route::get('/documentation', function () {
    return redirect('/#/documentation');
});

Route::resource('edit/project', 'EditorProjectController');
Route::resource('edit/skill', 'SkillsController');

Route::resource('api/project', 'ApiProjectController');
Route::resource('api', 'ApiController');
Route::resource('edit', 'EditorController');

/* Redirect to angular view */
Route::resource('/project', 'FolioProjectController');
Route::resource('/cat', 'FolioCategoryController');

Route::auth();

Route::get('/home', 'HomeController@index');
