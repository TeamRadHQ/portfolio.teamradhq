<?php
namespace Portfolio\Http\Controllers;

use Illuminate\Http\Request;

use Portfolio\Http\Requests;
use Portfolio\Project as Project;
use Portfolio\SkillCategory as Category;
use Portfolio\ProjectImage as Image;
use Portfolio\ProjectListItem as ListItem;
use Portfolio\ProjectLink as Link;
use Portfolio\ProjectSkill as ProjectSkill;
use Portfolio\Skill as Skill;
use Portfolio\ProjectType as Type;

class EditorProjectController extends Controller
{
    /**
     * @var Illuminate\Http\Request
     */
    protected $reqeuest;
    /** 
     * @var Portfolio/Project
     */
    protected $project;
    /** 
     * @var Portfolio/ProjectType (Collection)
     */
    protected $types;
    /** 
     * @var Portfolio/ProjectType (Collection)
     */
    protected $langs;
    /** 
     * @var Portfolio/Skill (Collection)
     */
    protected $techs;
    
    /**
     * Sets the request and determines which controller variables to set.  
     * @param Request $request
     */
    public function __construct(Request $request) {
        $this->middleware(['auth']);
        $this->request = $request;
        $this->types();
        if ($this->request->route()->getName() !== "edit.project.index") {
            $this->langs();
            $this->techs();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('edit.project.index', ['projectTypes' => $this->types()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('edit.project.create', $this->projectData());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->project = new Project;
        $this->saveProject();
        
        return redirect("/#/project/{$this->project->slug}")
            ->with(['flash_message' => $this->project->name . 'was added.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $this->project = Project::findBySlug($slug);

        return view('edit.project.edit', $this->projectData());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        // Get Project and clear skills.
        $this->project = Project::findBySlug($slug);
        $this->project->deskill();
        $this->saveProject();
    	return redirect("/#/project/{$this->project->slug}")
    		->with(['flash_message' => $this->project->name . 'was updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->project = Project::find($id);
        $this->project->deskill();
        $this->project->delete();

        return redirect()->route('edit.project.index')
            ->with(['flash_message' => $this->project->name . 'was deleted.']);
    }

   /**
     * Saves Project and any related models.
     * @param  Project $project
     * @return Project
     */
    public function saveProject() {
        $this->fillProject();
        $this->project->save();

        $this->saveListItems();
        $this->saveLinks();
        $this->saveImages();
        $this->saveLang();
        $this->saveTech();
        
        return $this;
    }

    public function saveImages() {
    	// Stop if there are no new or existing images.
        if ( $this->no_image_uploads() && 
        	!$this->project->images->count() ) {
        	return;
        }
        // Set request data to arrays.
        $images   = $this->request->file('images');
        $titles   = $this->request->get('imageTitles');
        $captions = $this->request->get('imageCaptions');
        
        // Loop through each image and save, update or clear
        foreach ($images as $key => $image ) {
        	// An image has been submitted
        	if( $image ) {
        		// dd('image submitted');
	        	$img = $this->imgSetup($key);

	        	$img->picture = $image;
	        	$img->title   = $titles[$key];
	        	$img->caption = $captions[$key];
	        	$this->project->images()->save($img);
        	// No image submitted, update titles and captions only
        	} else if (! $image && $titles[$key]) { 
        		$this->project->images[$key]->title   = $titles[$key];
        		$this->project->images[$key]->caption = $captions[$key];
        		$this->project->images[$key]->save();
        	// No image or data, clear image 
        	} else if (!$image && ! $titles[$key]) {
        		if ( isset($this->project->images[$key]) ) {
        			$this->project->images[$key]->delete();
        		}
        	}
        }
        
        return $this;
    }
    /**
     * Checks if the current Project has images[$key] 
     * and returns it or a new image if not.
     * @param  integer $key
     * @return Portfolio/ProjectImage
     */
    public function imgSetup($key) {
    	return (!isset($this->project->images[$key]) ? new Image : $this->project->images[$key]);
    }
    public function saveLinks() {
        Link::where('project_id', '=', $this->project->id)->delete();
        if ( count($this->request->linkTitle) ) {
            foreach($this->request->linkTitle as $key => $title) {
                if($title) {
                    Link::create([
                        "project_id" => $this->project->id,
                        "title"      => $title,
                        "url"        => $this->request->linkUrl[$key]
                    ]);
                }
            }
        }
        return $this;
    }
    /**
     * Loops through the list items and creates, updates, 
     * saves or deletes from the project.
     * @param  Request $request 
     * @param  Project $project 
     * @return Portfolio/Project
     */
    public function saveListItems() {
        foreach($this->request->listItem as $key => $listItem) {

            // Save or delete if a project listItem already exists.
            if (isset($this->project->listItems[$key])) {
                if($listItem) {
                    $this->project->listItems[$key]->list_item = $listItem;
                } else { 
                    $this->project->listItems[$key]->delete();
                }

            // Otherwise, save newly submitted ListItem.
            } else {
                if($listItem) {
                    $li = new ListItem;
                    $li->list_item = $listItem;
                    $this->project->listItems()->save($li);
                    unset($li);
                }
            }
        }

        return $this;
    }

    /**
     * Clears and saves Languages to Project Skills
     * @param  Request $request 
     * @param  Project $project 
     * @return Portfolio/Project
     */
    public function saveLang() {
        // Save project languages
        if(count($this->request->lang)) {
            foreach($this->request->lang as $skill) {
                $this->project->setSkill($skill);
            }
        }

        return $this;
    }

    /**
     * Clears and saves Technologies to Project Skills
     * @param  Request $request 
     * @param  Project $project 
     * @return Portfolio/Project
     */
    public function saveTech() {
        // Save project techs
        if(count($this->request->tech)) {
            foreach($this->request->tech as $skill) {
                $this->project->setSkill($skill);
            }
        }
        return $this;
    }

    /**
     * Creates a new Project or adds request data to existing
     * Project and returns. 
     * @param  Project $project 
     * @return Portfolio/Project
     */
    public function projectData() {
        if ( ! $this->project) {
            $this->project = new Project;
        }
        return [
            'project'      => $this->project,
            'projectTypes' => $this->types(), 
            'langs'        => $this->langs(),
            'techs'        => $this->techs(),
        ];
    }

    /**
     * Fills the Project with Request data.
     * @param  Project $project 
     * @param  Request $request 
     * @return Portfolio/Project           
     */
    public function fillProject() {
        $this->project->fill([
            'name'            => $this->request->get('name'),
            'description'     => $this->request->get('description'),
            'project_type_id' => $this->request->get('project_type_id'),
        ]);
        return $this;
    }

    /**
     * Gets the available Project Types from the database.
     * @return Portfolio/ProjectType (Collection)
     */
    public function types() {
        if( ! $this->types) {
            $this->types = Type::all();
        }
        return $this->types;
    }

    /**
     * Gets the available Project Types from the database.
     * @return Portfolio/Skill (Collection)
     */
    public function langs() {
        if ( ! isset($this->langs) ) {
            $this->langs = Category::where('id','<=',2)->with('skills')->get();
        }
        return $this->langs;
    }

    /**
     * Gets the available Project Tech Skills from the database.
     * @return Portfolio/Skill (Collection)
     */
    public function techs() { 
        if ( ! isset($this->techs) ) {
            $this->techs = Category::where('id', '>', 2)->with('skills')->get();
        }
        return $this->techs->toArray();
    }
    public function no_image_uploads() {
    	return empty( array_filter( $this->request->file('images') ) );
    }
}
