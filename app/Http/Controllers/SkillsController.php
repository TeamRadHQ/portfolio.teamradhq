<?php

namespace Portfolio\Http\Controllers;

use Illuminate\Http\Request;

use Portfolio\Http\Requests;
use Portfolio\Http\Requests\SkillsRequest as SkillsRequest;

use Portfolio\Skill as Skill;

class SkillsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "Skills Editor";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(SkillsRequest $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SkillsRequest $request)
    {
        $skill = new Skill;
        $skill->name = ucwords($request->get('name'));
        $skill->category_id = $request->get('category_id');
        $skill->rating = $request->get('rating');
        $skill->save();

        return redirect()->route('edit.index')
            ->with(['flash_message' => $skill->name . 'was added to ' . $skill->category->name]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $skill = Skill::find($id);
        $skill->name = ucwords($request->get('name'));
        $skill->category_id = $request->get('category_id');
        $skill->rating = $request->get('rating');
        $skill->save();

        return redirect()->route('edit.index')
            ->with(['flash_message' => $skill->name . 'has been updated.']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $skill = Skill::find($id);
        $skill->delete();
        return redirect()->route('edit.index')
            ->with(['flash_message' => $skill->name . 'was deleted from ' . $skill->category->name]);
    }
}
