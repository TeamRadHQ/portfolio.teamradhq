<?php

namespace Portfolio\Http\Controllers;

use Illuminate\Http\Request;

use Portfolio\Http\Requests;
use Portfolio\Project;
use Portfolio\ProjectType as Type;
use Portfolio\ProjectImage as Image;
use Portfolio\Skill;

class ApiProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->get('category')) {
            $data[] = Type::findBySlug($request->get('category'));    
            $data[0]["projects"] = Project::where('project_type_id', '=', $data[0]->id)
            						->select('name', 'slug', 'description')->get();
	        return response()->json($data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $project = Project::where('slug', '=', $slug)
        		->with('images', 'listItems', 'links', 'skills')
        		->first();
        
        return response()->json( $this->makeProjectArray($project) );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /**
     * Casts related models to array.
     * @param  Project $project 
     * @return Project
     */
    public function makeProjectArray(Project $project) {
        switch($project->project_type_id) {
        	case 1: 
        		$project["type"] = "Design";
        		break;
        	case 2: 
        		$project["type"] = "Development";
        		break;
        	case 3:
        		$project["type"] = "Documentation";
        		break;
        }

        unset($project->project_type_id);
        unset($project->created_at);
        unset($project->updated_at);
        unset($project->id);

        // Cast relations to array
        $project["list"]  = $project->listArray;
        $project["hrefs"] = $project->linkArray;
        $project["imgs"]  = $project->imgArray;
        $project["lang"]  = $project->langRatingArray;
        $project["tech"]  = $project->techRatingArray;
        // Clean up related models.
        unset($project->listItems);
        unset($project->links);
        unset($project->images);
        unset($project->skills);

        return $project;
    }

}
