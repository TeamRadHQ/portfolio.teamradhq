<?php

namespace Portfolio;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;



class SkillCategory extends Model
{
	use SluggableTrait;
	
	/**
     * Makes Project Type sluggable.
     * @var array
     */
    protected $sluggable = array(
        'build_from' => 'name',
        'save_to' => 'slug',
        'on_update' => true,
    );

    public function skills() {
        return $this->hasMany('Portfolio\Skill', 'category_id', 'id')->orderBy('name');
    }
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'slug'
    ];
    public function getAverageRatingAttribute() {
        return number_format($this->skills->average('rating'), 2);
    }
}
