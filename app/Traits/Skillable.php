<?php
namespace Portfolio\Traits;
use Portfolio\Skill;
use Portfolio\ProjectSkill;
use Portfolio\Providers\SkillService;
use Illuminate\Database\Eloquent\Builder;

/**
 * Trait Skillable
 * These traits can be implemented 
 * to attach Skills to Models.
 */
trait Skillable {
	/**
	 * Get a collection of all Skills a Model has.
	 * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
	 */
	public function skills() {

		return $this->morphToMany(Skill::class, 'skillable', 'project_skills')
			->orderBy('name')
			->withTimestamps();
	}
	/**
	 * Attach one or multiple Skills to a Model.
	 * @param  string|array $skills 
	 * @return $this
	 */
	public function setSkill($skills) {
		$skills = app(SkillService::class)->buildSkillArray($skills);
		foreach ($skills as $skillName) {
			$this->addOneSkill($skillName);
		}

		return $this->load('skills');
	}
	/**
	 * Detach one or multiple Skills from a Model.
	 * @param  string|array $skills
	 * @return $this
	 */
	public function unskill($skills) {
		$skills = app(SkillService::class)->buildSkillArray($skills);
		foreach($skills as $skillName) {
			$this->removeOneSkill($skillName);
		}

		return $this->load('skills');
	}
	/**
	 * Remove all Skills from a Model and attach 
	 * the supplied $skills.
	 * @param  string|array $skills
	 * @return $this
	 */
	public function reskill($skills) {

		return $this->deskill()->skill($skills);
	}
	/**
	 * Removes all Skills from the Model.
	 * @return $this
	 */
	public function deskill() {
		$pSkills = ProjectSkill::where('skillable_id', '=', $this->id)->delete();

		return $this->load('skills');
	}
	/**
	 * Add a Skill to the Model
	 * @param string $skillName
	 */
	public function addOneSkill($skillName) {
		$skill = app(SkillService::class)->find($skillName);

		if( ! $this->skills->contains($skill->getKey()) )  {
			$this->skills()->attach($skill->getKey());
		}
	}
	/**
	 * Remove one Skill from the Model.
	 * @param  string $skillName 
	 * @return void
	 */
	protected function removeOneSkill($skillName) {
		$skill = app(SkillService::class)->find($skillName);

		if($skill) {
			$this->skills()->detach($skill);
		}
	}
	public function getSkillListAttribute() {
		
		return app(SkillService::class)->makeSkillList($this);
	}
	public function getSkillListNormalisedAttribute() {
		
		return app(SkillService::class)->makeSkillList($this, 'normalised');
	}
	public function getSkillArrayAttribute() {

		return app(SkillService::class)->makeSkillArray($this);
	}
	public function getSkillArrayNormalisedAttribute() {

		return app(SkillService::class)->makeSkillArray($this, 'normalised');
	}
	public function scopeWithAllSkills(Builder $query, $skills) {
		$nomalised = app(SkillService::class)->buildSkillArrayNormalised($skills);

		return $query->has('skills', '=', count($nomalised), 'and', function(Builder $q) use ($normalised) {
			$q->whereIn('nomalised', $normalised);
		});
	}
	public function scopeWithAnySkills(Builder $query, $skills = []) {
		$nomalised = app(SkillService::class)->buildSkillArrayNormalised($skills);		

		if(empty($normalised)) {
			return $query->has('skills');
		}

		return $query->has('skills', '>', 0, 'and', function (Builder $q) use ($normalised) {
			$q->whereIn('normalised', $normalised);
		});
	}
	public function scopeWithoutSkills(Builder $query) {
		
		return $query->has('skills', '=', 0);
	}
	public static function allSkills() {
		$skills = app(SkillService::class)->getAllSkills(get_called_class());

		return $skills->plug('name')->sort()->all();
	}
	public static function allSkillsList() {

		return app(SkillService::class)->joinList(static::allSkills());
	}
}
