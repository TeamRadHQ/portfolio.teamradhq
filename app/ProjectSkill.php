<?php

namespace Portfolio;

use Illuminate\Database\Eloquent\Model;

class ProjectSkill extends Model
{
    protected $dates = ['created_at', 'updated_at'];
    
    public function skill() {
    	return $this->hasOne('Portfolio\Skill');
    }
}
