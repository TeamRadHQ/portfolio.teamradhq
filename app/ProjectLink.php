<?php

namespace Portfolio;

use Illuminate\Database\Eloquent\Model;

class ProjectLink extends Model
{
    protected $fillable = ['project_id', 'title', 'url'];
    protected $dates = ['created_at', 'updated_at'];
    public function project() {
    	return $this->belongsTo('Portfolio/Project');
    }
}
