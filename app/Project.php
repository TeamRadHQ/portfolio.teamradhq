<?php

namespace Portfolio;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Illuminate\Database\Eloquent\Model;
use Portfolio\Traits\Skillable;
use Portfolio\ProjectLink;

class Project extends Model
{
    use SluggableTrait;
    use Skillable;
    protected $sluggable = array(
        'build_from' => 'name',
        'save_to' => 'slug',
        'on_update' => true,
    );

    protected $fillable = ['name', 'description', 'project_type_id', 'slug'];

    protected $dates = ['created_at', 'updated_at'];
    /**
     * Broad Categorisation for project.
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function projectType() {
        return $this->belongsTo('Portfolio\ProjectType');
    }
    /**
     * Images attached to this project.
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function images() {
        return $this->hasMany('Portfolio\ProjectImage');
    }
    /**
     * Returns a multidimensional array of 
     * image titles, captions and urls.
     * @return array
     */
    public function getImgArrayAttribute() {
        $imgs=[];
        foreach($this->images as $img) {
        	$imgs[] = [
        		"id"        => $img->id,
        		"title"     => $img->title,
        		"caption"   => $img->caption,
        		"original"  => $img->picture->original->url,
        		"profile"   => $img->picture->profile->url,
        		"thumbnail" => $img->picture->thumbnail->url,
        	];
        }
        return $imgs;
    }
    /**
     * Links attached to the project.
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function links() {
        return $this->hasMany('Portfolio\ProjectLink');
    }
    /**
     * List items attached to the project.
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function listItems() {
        return $this->hasMany('Portfolio\ProjectListItem');
    }
    public function getListArrayAttribute() {
        $list=[];
        foreach($this->listItems as $item) {
            $list[] = $item->list_item;
        }
        return $list;
    }
    public function getLinkArrayAttribute() {
        $link=[];
        foreach($this->links as $item) {
            $link[] = [
                "title" => $item->title,
                "url"   => $item->url
            ];
        }
        return $link;
    }
    /**
     * Languages attached to this project.
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function lang() {
        return $this->skills->filter(function($skill)
            {
                if($skill->category_id <= 2)
                {
                    return $skill;
                }
         });
    }
    /**
     * Returns an array of Language names.
     * @return array
     */
    public function getLangArrayAttribute() {
        $lang=[];
        foreach($this->lang() as $item) {
            $lang[] = $item->name;
        }
        return $lang;
    }
    /**
     * Returns an array of Language names and ratings.
     * @return array
     */
    public function getLangRatingArrayAttribute() {
        $lang=[];
        // dd($this->lang());
        foreach($this->lang() as $item) {
            $lang[] = [
                "name"   => $item->name,
                "rating" => $item->rating
            ];
        }
        return $lang;
    }
    /**
     * Technologies attached to this project.
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function tech() {

        return $this->skills->filter(function($skill)
            {
                if($skill->category_id > 2)
                {
                    return $skill;
                }
         });
    }
    /**
     * Return an array of Technology names.
     * @return array
     */
    public function getTechArrayAttribute() {
        $tech=[];
        foreach($this->tech() as $item) {
            $tech[] = $item->name;
        }
        return $tech;
    }
    public function getTechRatingArrayAttribute() {
        $tech=[];
        foreach($this->tech() as $item) {
            $tech[] = [
                "name"   => $item->name,
                "rating" => $item->rating,
            ];
        }
        return $tech;
    }

    public function save(array $options = array()) {
        $this->resluggify();
        parent::save($options);
    }
}
