<?php

namespace Portfolio;

use Illuminate\Database\Eloquent\Model;

use SahusoftCom\EloquentImageMutator\EloquentImageMutatorTrait as ImageTrait;

class ProjectImage extends Model
{
	use ImageTrait;
	protected $image_fields = ['picture'];

	protected $fillable = ['project_id', 'picture', 'title', 'caption'];
	protected $dates = ['created_at', 'updated_at'];
    

    public function project() {
    	return $this->belongsTo('Portfolio\Project');
    }

}
