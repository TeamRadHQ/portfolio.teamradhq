<?php

namespace Portfolio;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Portfolio\Project;


class ProjectType extends Model
{
	use SluggableTrait;
	
	public $timestamps = false;

	/**
     * Makes Project Type sluggable.
     * @var array
     */
    protected $sluggable = array(
        'build_from' => 'name',
        'save_to' => 'slug',
        'on_update' => true,
    );

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description',
    ];

    public function projects() {
        return $this->hasMany('Portfolio\Project');
    }

}
