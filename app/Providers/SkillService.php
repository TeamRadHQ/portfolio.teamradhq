<?php
namespace Portfolio\Providers;

use Portfolio\Skill;
use Illuminate\Database\Eloquent\Model;

class SkillService {

	/**
	 * Find a skill by name.
	 * @param  String $skillName 
	 * @return Protfolio\Skill
	 */
	public function find($skillName) {
		$normalised = $this->normalise($skillName);
		return Skill::where('normalised', $normalised)->first();
	}
	
	/**
	 * Convert a delimited string into 
	 * an array of skill strings.
	 * @param  string|array $skills
	 * @return array
	 */
	public function buildSkillArray($skills) {
		if (is_string($skills)) {
			$skills = preg_split(
                '#[' . preg_quote(',', '#') . ']#',
                $skills,
                null,
                PREG_SPLIT_NO_EMPTY
            );
		}

		return (array)$skills;
	}
	/**
	 * Convert a delimted string into an array 
	 * of normalised skill strings.
	 * @param  string|array $skills
	 * @return array
	 */
	public function buildSkillArrayNormalised($skills) {
		$skills = $this->buildSkillArray($skills);
		
		return array_map([$this, 'normalise'], $skills);
	}
	/**
	 * Build a delimited string from a model's Skills.
	 * @param  Model  $model 
	 * @param  string $field
	 * @return string
	 */
	public function makeSkillList(Model $model, $field = 'name') {
		$skills = $this->makeSkillArray($model, $field);

		return $this->joinList($skills);
	}

	/**
	 * Glues a list of strings together.
	 * @param  array  $array
	 * @return string
	 */
	public function joinList(array $array) {

		return implode(', ', $array);
	}
	/**
	 * Build a simple array of a model's skills.
	 * @param  Model  $model 
	 * @param  string $field 
	 * @return array
	 */
	public function makeSkillArray(Model $model, $field = 'name') {
		$skills = $model->skills;

		return $skills->pluck($field)->all();
	}

	public function normalise($string) {
		if($string == "node-js") $string = "node.js";
		return str_replace('-',' ', mb_strtolower($string) );
	}
	/**
	 * Get all skills for the given class.
	 * @param  Illuminate\Database\Eloquent\Model|string $class
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function getAllSkills($class) {
		if ($class instanceof Model) {
			$class = get_class($class);
		}

		$sql = 'SELECT DISTINCT s.*' . 
			' FROM project_skills ps LEFT JOIN skills s ON ps.skill_id = s.id' .
			' WHERE ss.skillable_type = ?';

		return Skill::hydrateRaw($slq, [$class]);
	}

}