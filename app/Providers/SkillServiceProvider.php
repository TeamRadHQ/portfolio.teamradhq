<?php

namespace Portfolio\Providers;
use Illuminate\Support\ServiceProvider;
use Portfolio\Providers\SkillService;

class SkillServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SkillService::class);
    }
}
