<?php

namespace Portfolio;

use Illuminate\Database\Eloquent\Model;

class ProjectListItem extends Model
{
    protected $dates = ['created_at', 'updated_at'];
    protected $fillable = ["project_id", "list_item"];
    public function projectType() {
    	return $this->hasOne('Portfolio/ProjectType');
    }
    public function project() {
    	return $this->belongsTo('Portfolio/Project');
    }
}
